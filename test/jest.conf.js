const path = require('path')

module.exports = {
  rootDir: path.resolve(__dirname, '../'),
  coverageDirectory: '<rootDir>/test/coverage',
  collectCoverageFrom: [
    'twilio/**/*.{js}',
    '!twilio/**/*.routes.js',
    '!server.js',
    '!routes.js'
  ],
  resetMocks: true,
  coverageReporters: ['lcov'],
  coverageThreshold: {
    global: {
      statements: 100,
      branches: 90.91,
      functions: 33.33,
      lines: 100
    }
  },
  testEnvironment: 'node',
  transform: {
    '^.+\\.js$': '<rootDir>/test/preprocessor.js'
  },
  transformIgnorePatterns: [
    'node_modules/(?!loopback-boot)'
  ]
}
