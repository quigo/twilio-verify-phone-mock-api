const app = require('express')()
require('dotenv').config()
const cors = require('cors')
const corsOptions = {
  'origin': '*',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  'preflightContinue': false,
  'optionsSuccessStatus': 204
}
app.use(cors(corsOptions))

const bodyparser = require('body-parser')
app.use(bodyparser.json())

const routes = require('./routes')
routes(app)

app.listen(process.env.API_PORT, () => {
  // eslint-disable-next-line no-console
  console.info(`Mock API is running on port ${process.env.API_PORT}`)
}
)
