module.exports = {
  apps: [
    {
      name: 'twilio-mock-api',
      script: 'server.js',
      watch: false,
      'log_file': './logs/combined.outerr.log',
      'out_file': './logs/out.log',
      'error_file': './logs/err.log',
      env: {
        NODE_ENV: 'development'
      }
    }
  ]
}
