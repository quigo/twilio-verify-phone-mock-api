const twilioMiddleware = require('./mock-api.middleware')

const twilioServices = require('./mock-api.services')
jest.mock('./mock-api.services')

describe('twilio.mock-api.middleware', () => {
  beforeEach(() => {
    jest.resetModules()
  })

  describe('verify-phone', () => {
    test('should send http response 200 with {status: true} as payload when phone number is valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => true
      const req = { body: { phone: '+237898956' } }
      const res = { send: jest.fn() }

      // WHEN
      twilioMiddleware.verifyPhone(req, res)

      // THEN
      expect(res.send).toHaveBeenCalledWith({ status: true })
    })

    test('should not send {status: true} payload when phone number is not valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => false
      const req = { body: { phone: '+237898999' } }
      const res = { send: jest.fn() }
      const next = jest.fn()

      // WHEN
      twilioMiddleware.verifyPhone(req, res, next)

      // THEN
      expect(res.send).not.toHaveBeenCalled()
    })

    test('should call next error middleware with specific error message when phone number is not valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => false
      const req = { body: { phone: '+237898999' } }
      const next = jest.fn()

      // WHEN
      twilioMiddleware.verifyPhone(req, undefined, next)

      // THEN
      expect(next).toHaveBeenCalledWith({ message: 'Error occured when trying to verify phone number. Please try again later' })
    })
  })

  describe('check-verification-token', () => {
    test('should send http response 200 with {status: true} as payload when phone number and token are valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => true
      twilioServices.isValidToken = () => true
      const req = { body: { phone: '+237898956', code: 1235 } }
      const res = { send: jest.fn() }

      // WHEN
      twilioMiddleware.checkToken(req, res)

      // THEN
      expect(res.send).toHaveBeenCalledWith({ status: true })
    })

    test('should not send {status: true} payload when phone number is not valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => false
      const req = { body: { phone: '+237898999', code: 1235 } }
      const res = { send: jest.fn() }
      const next = jest.fn()

      // WHEN
      twilioMiddleware.checkToken(req, res, next)

      // THEN
      expect(res.send).not.toHaveBeenCalled()
    })

    test('should not send {status: true} payload when phone is valid and token is not valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => true
      twilioServices.isValidToken = () => false

      const req = { body: { phone: '+237898999', code: 1235 } }
      const res = { send: jest.fn() }
      const next = jest.fn()

      // WHEN
      twilioMiddleware.checkToken(req, res, next)

      // THEN
      expect(res.send).not.toHaveBeenCalled()
    })

    test('should call next error middleware with specific error message when phone number is not valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => false
      const req = { body: { phone: '+237898999', code: 1235 } }
      const next = jest.fn()

      // WHEN
      twilioMiddleware.checkToken(req, undefined, next)

      // THEN
      expect(next).toHaveBeenCalledWith({ message: 'The code you submitted is invalid. Please try again.' })
    })

    test('should call next error middleware with specific error message when phone is valid and token not valid', async () => {
      // GIVEN
      twilioServices.isValidPhone = () => true
      twilioServices.isValidToken = () => false
      const req = { body: { phone: '+237898999', code: 1235 } }
      const next = jest.fn()

      // WHEN
      twilioMiddleware.checkToken(req, undefined, next)

      // THEN
      expect(next).toHaveBeenCalledWith({ message: 'The code you submitted is invalid. Please try again.' })
    })
  })
})
