const twilioMiddleware = require('./mock-api.middleware')
const twilioRoutes = require('express').Router()

const errorHandler = (error, _req, res, _next) => {
  res.status(400).json(error)
}

twilioRoutes.route('/verify-phone').post(twilioMiddleware.verifyPhone, errorHandler)
twilioRoutes.route('/check-verification-token').post(twilioMiddleware.checkToken, errorHandler)

module.exports = twilioRoutes
