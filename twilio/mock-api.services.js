module.exports = {
  isValidPhone: function (phoneWithSpace = ' ') {
    const phone = phoneWithSpace.replace(/ /g, '')
    const validPhoneRegx = new RegExp(process.env.VALID_PHONE_NUMBER_PATTERN)
    const notValidPhoneRegx = new RegExp(process.env.NOT_VALID_PHONE_NUMBER_PATTERN)
    return notValidPhoneRegx.exec(phone) ? false : !!validPhoneRegx.exec(phone)
  },
  isValidToken: function (token) {
    const validTokenRegx = new RegExp(process.env.APPROVED_TOKEN)
    const notValidTokenRegx = new RegExp(process.env.FAILED_TOKEN)
    return notValidTokenRegx.exec(token) ? false : !!validTokenRegx.exec(token)
  }
}
