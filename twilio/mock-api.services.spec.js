const twilioServices = require('./mock-api.services')
describe('twilio.mock-api.services', () => {
  beforeEach(() => {
    const OLD_ENV = process.env

    beforeEach(() => {
    })

    afterEach(() => {
      process.env = OLD_ENV
    })
  })

  describe('isValidPhone', () => {
    test('should return true when phone matches the valid phone number pattern', async () => {
      // GIVEN
      process.env.NOT_VALID_PHONE_NUMBER_PATTERN = '^\\+33725202025$'
      process.env.VALID_PHONE_NUMBER_PATTERN = '^\\+2379965623$'

      // WHEN
      const actual = twilioServices.isValidPhone('+2379965623')

      // THEN
      expect(actual).toBe(true)
    })

    test('should return true when phone with space matches the valid phone number pattern', async () => {
      // GIVEN
      process.env.NOT_VALID_PHONE_NUMBER_PATTERN = '^\\+33725202025$'
      process.env.VALID_PHONE_NUMBER_PATTERN = '^\\+2379965623$'

      // WHEN
      const actual = twilioServices.isValidPhone('+237 9 96 56 23')

      // THEN
      expect(actual).toBe(true)
    })

    test('should return false when phone matches the not valid phone number pattern', async () => {
      // GIVEN
      process.env.NOT_VALID_PHONE_NUMBER_PATTERN = '^\\+33725202025$'
      process.env.VALID_PHONE_NUMBER_PATTERN = '^\\+2379965623$'

      // WHEN
      const actual = twilioServices.isValidPhone('+33725202025')

      // THEN
      expect(actual).toBe(false)
    })

    test('should return false when phone matches the not valid and the valid phone number pattern', async () => {
      // GIVEN
      process.env.NOT_VALID_PHONE_NUMBER_PATTERN = '^\\+33725202025$'
      process.env.VALID_PHONE_NUMBER_PATTERN = '^\\+33725202025$'

      // WHEN
      const actual = twilioServices.isValidPhone('+33725202025')

      // THEN
      expect(actual).toBe(false)
    })
  })

  describe('isValidToken', () => {
    test('should return true when token matches the valid phone number pattern', async () => {
      // GIVEN
      process.env.APPROVED_TOKEN = '\\d{4,10}$'
      process.env.FAILED_TOKEN = '^23007$'

      // WHEN
      const actual = twilioServices.isValidToken(25007)

      // THEN
      expect(actual).toBe(true)
    })

    test('should return false when token matches the not valid token pattern', async () => {
      // GIVEN
      process.env.APPROVED_TOKEN = '^23007$'
      process.env.FAILED_TOKEN = '^25007$'

      // WHEN
      const actual = twilioServices.isValidToken(25007)

      // THEN
      expect(actual).toBe(false)
    })

    test('should return false when token matches the valid token and the not valid token pattern', async () => {
      // GIVEN
      process.env.APPROVED_TOKEN = '\\d{4,10}$'
      process.env.FAILED_TOKEN = '^23007$'

      // WHEN
      const actual = twilioServices.isValidToken(23007)

      // THEN
      expect(actual).toBe(false)
    })
  })
})
