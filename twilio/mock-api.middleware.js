const twilioServices = require('./mock-api.services')
const validStatus = { status: true }
module.exports = {
  verifyPhone: function ({ body: { phone } }, res, next) {
    twilioServices.isValidPhone(phone)
      ? res.send(validStatus)
      : next({ message: 'Error occured when trying to verify phone number. Please try again later' })
  },
  checkToken: function ({ body: { phone, code } }, res, next) {
    twilioServices.isValidPhone(phone) && twilioServices.isValidToken(code)
      ? res.send(validStatus)
      : next({ message: 'The code you submitted is invalid. Please try again.' })
  }
}
