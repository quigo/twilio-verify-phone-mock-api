const twilioMockRoutes = require('./twilio/mock-api.routes')

module.exports = (app) => {
  app.use(twilioMockRoutes)
}
