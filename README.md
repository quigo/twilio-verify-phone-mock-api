# Twilio verify phonenumber mock API

## Context

This has been built cause twilio actually does not offer in his sandbox an API to test phone number verfification

### packages manager

You need to install **npm** package manager then install **yarn**

``` bash
npm install -g yarn
```

### install dependencies

``` bash
yarn install
```

## Build package

``` bash
yarn build
```

Create dist/ directory which contains needed sources and dependencies for production.

## Tests

Launch unit test

``` bash
yarn test
```

Launch test with coverage threshold

``` bash
yarn test:coverage
```

## Code style check

Run eslint linter

``` bash
yarn lint
```

## Run app

Run app with watcher on files. If a file changes the app is restart.

``` bash
yarn start:dev
```

Run without watcher

``` bash
yarn start
```

## Run in production

First step install pm2

``` bash
yarn global add pm2
```

Then build production package. The command will create dist directory containing everything needed to run the nodejs app.

``` bash
yarn Build
```

In the dist directory run :

``` bash
pm2-runtime start process.config.js
```

## How it works

To make it easy to use with keep the Twilio api endpoint path for phone verification and validation.

We do not check the phone format validity we leave it to you.

You will find a *.env-example* file you will use to create the *.env* file which will be used by the running app.

### Phone number verification

 By setting the phone number pattern in appropriate environment variable **VALID_PHONE_NUMBER_PATTERN**. This will allow all validate phonenumber to be considered valid. For not valid phonenumber we will set another environment variable **NOT_VALID_PHONE_NUMBER_PATTERN**.

We propose default patterns :

- **VALID_PHONE_NUMBER_PATTERN**  ```\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$``` which matchs generic international phone number without space between digits thanks to [https://phoneregex.com](https://phoneregex.com)

- **NOT_PHONE_NUMBER_PATTERN** ```\+33725202025$``` wich is an unique french phone number: +33725202025. Up to you change it to handle several cases.

Then you just need to call the endpoint with respect to HTTP verb and payload :

**POST** [http://localhost:3000/verify-phone](http://localhost:3000/verify-phone)

Payload

```JSON
{
    "phone": "+2379995689"
}
```

Response when succeed **HTTP 200** :white_check_mark:

```JSON
{
    "status": true
}
```

Response when failed **HTTP 400** :x:

Expected when sending **+33725202025** phone number.

```JSON
{
    "message": "Error occured when trying to verify phone number. Please try again later"
}
```

### Phone number token checking

We set an environment variable pattern to handle approved, expired and pending token with default value.

- **APPROVED_TOKEN** ```\d{4,10}$``` This handle token with at least 4 to maximum 10 digits as valid token.

- **FAILED_TOKEN** ```\25007$``` This will cause token checking to failed.

Then you just need to call the endpoint with respect to HTTP verb and payload :

**POST** [http://localhost:3000/check-verification-token](http://localhost:3000/check-verification-token)

Payload

```JSON
{
    "phone": "+2379995689",
    "code" : 6598
}
```

Response when succeed **HTTP 200** :white_check_mark:

```JSON
{
    "status": true
}
```

Response when failed **HTTP 400** :x:

Expected when sending **25007** token in code property of the payload.

```JSON
{
   "message": "The code you submitted is invalid. Please try again."
}
```
